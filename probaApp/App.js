/**
 * Proba App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AdsList from './src/screens/AdsList';
import AdDetails from './src/screens/AdDetails';
import Slider from './src/screens/Slider';
import AddAd from './src/screens/AddAd';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <StatusBar
          translucent
          backgroundColor="#008073"
          barStyle="light-content"
        />
        <Stack.Navigator>
          <Stack.Screen
            options={{headerShown: false}}
            name="AdsList"
            component={AdsList}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="AdDetails"
            component={AdDetails}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="Slider"
            component={Slider}
          />
          <Stack.Screen
            options={{headerShown: false}}
            name="AddAd"
            component={AddAd}
          />
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}

export default App;
