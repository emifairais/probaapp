import * as React from 'react';
import {View, TouchableWithoutFeedback, StyleSheet, Text} from 'react-native';
import {Icon} from 'react-native-elements';
import {SliderBox} from 'react-native-image-slider-box';
import {colors} from '../configs/colors';
import {baseURL} from '../configs/constant';

function AdCard(props) {
  const {ad, navigation, isListItem, sliderHeight} = props;

  const [imageIndex, setImageIndex] = React.useState(1);

  const urlImages = [];
  ad.images.forEach((url) => {
    const fullUrl = baseURL + '/storage/' + url;
    urlImages.push(fullUrl);
  });

  return (
    <View>
      <TouchableWithoutFeedback
        onPress={() => {
          if (isListItem) {
            navigation.navigate('AdDetails', {
              data: ad,
            });
          }
        }}>
        <View styles={styles.cardContainer}>
          <SliderBox
            images={urlImages}
            imageLoadingColor={colors.green}
            sliderBoxHeight={sliderHeight}
            dotColor={colors.green}
            paginationBoxVerticalPadding={7}
            dotStyle={styles.dotStyle}
            currentImageEmitter={(index) => {
              setImageIndex(index + 1);
            }}
            onCurrentImagePressed={() => {
              if (isListItem) {
                navigation.navigate('AdDetails', {
                  data: ad,
                });
              } else {
                navigation.navigate('Slider', {
                  data: ad,
                });
              }
            }}
          />
          <View style={styles.indexContainer}>
            <Text style={styles.indexText}>
              {imageIndex}/{ad.images.length}
            </Text>
          </View>
          <Text style={styles.priceText}>
            {ad.price} {ad.currency}
          </Text>
          <Text style={styles.titleText}>{ad.title}</Text>
          {isListItem ? (
            <Text style={styles.descripText} numberOfLines={3}>
              {ad.description}
            </Text>
          ) : null}
          <View style={[styles.bottomRow, {paddingTop: !isListItem ? 15 : 0}]}>
            <View style={styles.column}>
              <Icon
                color="rgba(200, 200, 200, 0.9)"
                name="location-pin"
                type="entypo"
                size={18}
              />
              <Text style={styles.locationText}>{ad.county}</Text>
            </View>
            <View style={styles.column}>
              <Text style={styles.dateText}>{ad.date}</Text>
              <Icon
                color="rgba(200, 200, 200, 0.9)"
                name="clock-time-three-outline"
                type="material-community"
                size={18}
              />
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
      <View
        style={{
          backgroundColor: 'rgba(217, 217, 217, 0.4)',
          height: 10,
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  cardContainer: {
    flex: 1,
  },
  indexContainer: {
    position: 'absolute',
    right: 15,
    top: 10,
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    width: 50,
    height: 30,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  indexText: {
    color: 'white',
    fontSize: 17,
  },
  priceText: {
    color: colors.green,
    fontSize: 18,
    fontWeight: 'bold',
    paddingLeft: 20,
    paddingTop: 8,
    paddingBottom: 5,
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    paddingLeft: 20,
    paddingRight: 80,
    paddingBottom: 9,
  },
  descripText: {
    color: 'rgba(0, 0, 0, 0.7)',
    fontSize: 13,
    paddingLeft: 20,
    paddingRight: 30,
    paddingBottom: 15,
  },
  bottomRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginBottom: 15,
  },
  locationText: {
    color: 'rgba(0, 0, 0, 0.4)',
    fontSize: 12,
    paddingTop: 2,
  },
  dateText: {
    color: 'rgba(0, 0, 0, 0.4)',
    fontSize: 12,
    paddingRight: 7,
    paddingTop: 1,
  },
  column: {
    flexDirection: 'row',
  },
  dotStyle: {
    width: 8,
    height: 8,
    borderRadius: 8,
    marginHorizontal: -5,
  },
});

export default AdCard;
