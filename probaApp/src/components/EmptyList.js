import * as React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {colors} from '../configs/colors';

function EmptyList(props) {
  const {type, imagePath, size, index, addImage} = props;
  return (
    <View style={styles.container}>
      <Image
        source={require('../assets/images/broke.png')}
        style={styles.image}
      />
      <Text style={styles.emptyText}>
        Oops! Nu există anunțuri disponibile :(
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
  image: {
    width: '70%',
    height: '50%',
    resizeMode: 'contain',
    alignSelf: 'center',
    // borderRadius: 10,
  },
  emptyText: {
    alignSelf: 'center',
    textAlign: 'center',
    paddingTop: 60,
    paddingHorizontal: 40,
    color: colors.green,
    fontFamily: 'serif',
    fontWeight: 'bold',
  },
});

export default EmptyList;
