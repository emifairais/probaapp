import * as React from 'react';
import {View, Image, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import {colors} from '../configs/colors';

function ImageCard(props) {
  const {type, imagePath, size, index, addImage} = props;
  return (
    <View
      style={{
        height: size,
        width: size,
        borderRadius: 10,
        backgroundColor: '#ececec',
        marginTop: index <= 2 ? 10 : 2,
        marginBottom: index >= 6 ? 10 : 2,
        marginHorizontal: 2,
        marginVertical: 2,
        borderWidth: type === 'add' ? 2.2 : 0,
        borderColor: colors.green,
      }}>
      {type === 'image' ? (
        <Image source={{uri: imagePath.uri}} style={styles.image} />
      ) : null}
      {type === 'add' ? (
        <TouchableOpacity style={styles.plusButton} onPress={() => addImage()}>
          <Icon
            name="pluscircleo"
            type="antdesign"
            color={colors.green}
            size={40}
            style={styles.icon}
          />
        </TouchableOpacity>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    borderRadius: 10,
  },
  plusButton: {
    backgroundColor: 'white',
    height: '100%',
    borderRadius: 10,
  },
  icon: {
    paddingTop: '30%',
  },
});

export default ImageCard;
