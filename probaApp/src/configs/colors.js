export const colors = {
  green: '#009587',
};

export const maskColors = [
  '#f2f2f2',
  'rgba(255,255,255,1)',
  'rgba(255,255,255,0.3)',
  'rgba(255,255,255,0.1)',
];

export const linearGradientColor = ['#f2f2f2', '#f2f2f2'];
