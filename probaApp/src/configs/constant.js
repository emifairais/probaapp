export const maxImages = 9;

export const baseURL = 'http://192.168.100.4:8000';

export const counties = [
  {
    label: 'Bucureşti',
    value: 'Bucureşti',
  },
  {
    label: 'Alba',
    value: 'Alba',
  },
  {
    label: 'Arad',
    value: 'Arad',
  },
  {
    label: 'Argeș',
    value: 'Argeș',
  },
  {
    label: 'Bacău',
    value: 'Bacău',
  },
  {
    label: 'Bihor',
    value: 'Bihor',
  },
  {
    label: 'Bistriţa-Năsăud',
    value: 'Bistriţa-Năsăud',
  },
  {
    label: 'Botoşani',
    value: 'Botoşani',
  },
  {
    label: 'Braşov',
    value: 'Braşov',
  },
  {
    label: 'Brăila',
    value: 'Brăila',
  },
  {
    label: 'Buzău',
    value: 'Buzău',
  },
  {
    label: 'Caraş-Severin',
    value: 'Caraş-Severin',
  },
  {
    label: 'Călăraşi',
    value: 'Călăraşi',
  },
  {
    label: 'Cluj',
    value: 'Cluj',
  },
  {
    label: 'Constanţa',
    value: 'Constanţa',
  },
  {
    label: 'Covasna',
    value: 'Covasna',
  },
  {
    label: 'Dâmboviţa',
    value: 'Dâmboviţa',
  },
  {
    label: 'Dolj',
    value: 'Dolj',
  },
  {
    label: 'Bacău',
    value: 'Bacău',
  },
  {
    label: 'Galaţi',
    value: 'Galaţi',
  },
  {
    label: 'Giurgiu',
    value: 'Giurgiu',
  },
  {
    label: 'Gorj',
    value: 'Gorj',
  },
  {
    label: 'Harghita',
    value: 'Harghita',
  },
  {
    label: 'Hunedoara',
    value: 'Hunedoara',
  },
  {
    label: 'Brăila',
    value: 'Brăila',
  },
  {
    label: 'Ialomiţa',
    value: 'Ialomiţa',
  },
  {
    label: 'Iaşi',
    value: 'Iaşi',
  },
  {
    label: 'Ilfov',
    value: 'Ilfov',
  },
  {
    label: 'Maramureş',
    value: 'Maramureş',
  },
  {
    label: 'Mehedinţi',
    value: 'Mehedinţi',
  },
  {
    label: 'Mureş',
    value: 'Mureş',
  },
  {
    label: 'Neamţ',
    value: 'Neamţ',
  },
  {
    label: 'Olt',
    value: 'Olt',
  },
  {
    label: 'Prahova',
    value: 'Prahova',
  },
  {
    label: 'Satu Mare',
    value: 'Satu Mare',
  },
  {
    label: 'Sălaj',
    value: 'Sălaj',
  },
  {
    label: 'Sibiu',
    value: 'Sibiu',
  },
  {
    label: 'Suceava',
    value: 'Suceava',
  },
  {
    label: 'Teleorman',
    value: 'Teleorman',
  },
  {
    label: 'Timiş',
    value: 'Timiş',
  },
  {
    label: 'Tulcea',
    value: 'Tulcea',
  },
  {
    label: 'Vâlcea',
    value: 'Vâlcea',
  },
  {
    label: 'Vaslui',
    value: 'Vaslui',
  },
  {
    label: 'Vrancea',
    value: 'Vrancea',
  },
];
