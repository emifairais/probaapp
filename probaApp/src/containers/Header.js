import * as React from 'react';
import {StyleSheet} from 'react-native';
import {Header} from 'react-native-elements';
import {colors} from '../configs/colors';

function HeaderComponent(props) {
  const {...rest} = props;
  return <Header {...rest} containerStyle={styles.container} />;
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.green,
    borderBottomColor: colors.green,
    borderBottomWidth: 1,
    height: 88,
  },
});

export default HeaderComponent;
