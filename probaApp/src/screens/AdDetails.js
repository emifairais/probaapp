import * as React from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Header from '../containers/Header';
import AdCard from '../components/AdCard';
import {Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import MaskedView from '@react-native-community/masked-view';
import {maskColors, linearGradientColor} from '../configs/colors';

function AdDetails(props) {
  const {navigation, route} = props;
  const ad = route.params.data;

  const [textShown, setTextShown] = React.useState(false);
  const [lengthMore, setLengthMore] = React.useState(false);
  const toggleNumberOfLines = () => {
    setTextShown(!textShown);
  };

  const onTextLayout = React.useCallback((e) => {
    setLengthMore(e.nativeEvent.lines.length >= 5);
  }, []);

  const Component = lengthMore ? ScrollView : View;

  return (
    <View style={{height: '100%', backgroundColor: 'white'}}>
      <Header
        leftComponent={
          <Icon
            name="chevron-back"
            size={30}
            type="ionicon"
            color="white"
            onPress={() => navigation.goBack()}
          />
        }
        centerComponent={
          <View style={styles.headerTitle}>
            <Text>
              <Text style={styles.firstText}>Detalii anunț</Text>
            </Text>
          </View>
        }
      />
      <Component>
        <AdCard
          component={TouchableWithoutFeedback}
          ad={ad}
          style={styles.adCard}
          hasDescription={false}
          isListItem={false}
          sliderHeight={250}
          navigation={navigation}
        />
        {lengthMore && !textShown ? (
          <MaskedView
            maskElement={
              <LinearGradient colors={maskColors} style={{flex: 1}} />
            }>
            <LinearGradient colors={linearGradientColor} style={{flex: 1}}>
              <View style={styles.descriptionContainer}>
                <Text style={styles.descriptionTitle}>Descriere</Text>
                <Text
                  style={styles.descriptionText}
                  onTextLayout={onTextLayout}
                  numberOfLines={textShown ? undefined : 5}>
                  {ad.description}
                </Text>
              </View>
            </LinearGradient>
          </MaskedView>
        ) : (
          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionTitle}>Descriere</Text>
            <Text
              style={styles.descriptionText}
              onTextLayout={onTextLayout}
              numberOfLines={textShown ? undefined : 5}>
              {ad.description}
            </Text>
          </View>
        )}
      </Component>
      {lengthMore && !textShown ? (
        <TouchableOpacity
          onPress={toggleNumberOfLines}
          style={styles.addButton}
          activeOpacity={0.8}>
          <Text style={styles.buttonText}>Vezi mai mult</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  detailComponent: {
    marginBottom: 30,
  },
  headerTitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  firstText: {
    color: 'white',
    fontSize: 20,
  },
  secondText: {
    fontFamily: 'MontserratAlternates-Bold',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 12,
  },
  icon: {
    height: 38,
    width: 38,
    borderRadius: 20,
    marginRight: 2,
  },
  descriptionContainer: {
    paddingLeft: 20,
    paddingBottom: 30,
  },
  descriptionTitle: {
    fontWeight: 'bold',
    fontSize: 17,
    paddingVertical: 15,
  },
  descriptionText: {
    fontSize: 15,
    paddingRight: 15,
    lineHeight: 20,
  },
  addButton: {
    width: 150,
    height: 38,
    borderRadius: 20,
    position: 'absolute',
    bottom: 25,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'rgba(217, 217, 217, 0.8)',
    borderWidth: 2,
  },
  buttonText: {
    fontSize: 14,
    color: 'rgba(0, 0, 0, 0.5)',
  },
});

export default AdDetails;
