import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import Header from '../containers/Header';
import {Icon} from 'react-native-elements';
import {colors} from '../configs/colors';
import {addAd} from '../services/ads-services';
import {updateImagesCards, renderCards} from '../services/add-functions';
import {counties} from '../configs/constant';
import Toggle from 'react-native-toggle-element';
import DropDownPicker from 'react-native-dropdown-picker';
import ToggleSwitch from 'toggle-switch-react-native';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';

function AddAd(props) {
  const {navigation} = props;
  const [title, setTitle] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [price, setPrice] = React.useState('');
  const [currency, setCurrency] = React.useState('RON');
  const [county, setCounty] = React.useState('');
  const [titleCount, setTitleCount] = React.useState(38);
  const [descriptionCount, setDescriptionCount] = React.useState(5000);
  const [toggleValue, setToggleValue] = React.useState(false);
  const [termsValue, setTermsValue] = React.useState(false);
  const [termsColor, setTermsColor] = React.useState('rgba(0, 0, 0, 0.7)');
  const [imagesExist, setImagesExist] = React.useState(false);
  const [images, setImages] = React.useState([]);
  const [imagesCards, setImagesCards] = React.useState([]);

  React.useEffect(() => {
    updateImagesCards(images, setImagesCards);
  }, []);

  const chooseImage = () => {
    let options = {
      title: 'Selectează imaginea',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    return new Promise(() => {
      ImagePicker.showImagePicker(options, (response) => {
        if (response.didCancel) {
          Alert.alert(
            'Întrerupere acțiune',
            'Acțiunea de alegere imagine a fost întreruptă de către utilizator!',
          );
        } else if (response.error) {
          Alert.alert('Eroare imagine', response.error);
        } else {
          const imagesArray = images;
          imagesArray.push({
            uri: response.uri,
            fileName: response.fileName,
            type: response.type,
          });
          setImages(imagesArray);
          updateImagesCards(imagesArray, setImagesCards);
          !imagesExist ? setImagesExist(true) : null;
        }
      });
    });
  };

  return (
    <View style={{height: '100%', backgroundColor: 'white'}}>
      <Header
        leftComponent={
          <Icon
            name="chevron-back"
            size={30}
            type="ionicon"
            color="white"
            onPress={() => navigation.goBack()}
          />
        }
        centerComponent={
          <View style={styles.headerTitle}>
            <Text>
              <Text style={styles.firstText}>Adaugă anunț</Text>
            </Text>
          </View>
        }
      />
      <ScrollView>
        <View
          style={[
            styles.photoContainer,
            {flexWrap: 'wrap', alignItems: 'flex-start'},
          ]}>
          {!imagesExist ? (
            <View
              style={[
                styles.photoContainer,
                {flexDirection: 'column', paddingTop: 25},
              ]}>
              <View style={styles.photoButtonContainer}>
                <Icon
                  name="camera-plus-outline"
                  size={32}
                  type="material-community"
                  onPress={() => chooseImage()}
                  color="white"
                />
              </View>
              <Text style={styles.addImagesText}>Adaugă imagini</Text>
            </View>
          ) : (
            renderCards(imagesCards, chooseImage)
          )}
        </View>
        <View style={styles.separator} />
        <View style={styles.titleContainer}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.titleText}>Titlul anunțului</Text>
            <Text style={styles.titleCountText}>{titleCount}</Text>
          </View>
          <TextInput
            style={styles.titleInput}
            maxLength={38}
            onChangeText={(text) => {
              setTitle(text);
              setTitleCount(38 - text.length);
            }}
          />
        </View>
        <View style={styles.separator} />
        <View style={styles.titleContainer}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.titleText}>Descrierea anunțului</Text>
            <Text style={styles.titleCountText}>{descriptionCount}</Text>
          </View>
          <TextInput
            style={[styles.titleInput, styles.descriptionInput]}
            maxLength={5000}
            onChangeText={(text) => {
              setDescription(text);
              setDescriptionCount(5000 - text.length);
            }}
            multiline={true}
          />
        </View>
        <View style={styles.separator} />
        <View style={styles.titleContainer}>
          <Text style={styles.titleText}>Preț</Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <TextInput
              value={price}
              keyboardType={'phone-pad'}
              style={styles.priceInput}
              onChangeText={(text) => {
                setPrice(text.replace(/[^0-9]/g, ''));
              }}
            />
            <Toggle
              value={toggleValue}
              onPress={(newState) => {
                newState ? setCurrency('EUR') : setCurrency('RON');
                setToggleValue(newState);
              }}
              trackBar={{
                activeBackgroundColor: 'rgb(240, 240, 240)',
                inActiveBackgroundColor: 'rgb(240, 240, 240)',
                width: 92,
                height: 41,
                radius: 20,
              }}
              containerStyle={{paddingBottom: 15}}
              thumbButton={{
                activeBackgroundColor: colors.green,
                inActiveBackgroundColor: colors.green,
                width: 46,
                height: 39,
                radius: 20,
              }}
              leftComponent={
                <Text
                  style={{
                    fontSize: 16,
                    color: !toggleValue
                      ? 'rgb(250, 250, 250)'
                      : 'rgb(80, 80, 80)',
                    fontWeight: !toggleValue ? 'bold' : 'normal',
                    paddingLeft: toggleValue ? 3 : 0,
                  }}>
                  RON
                </Text>
              }
              rightComponent={
                <Text
                  style={{
                    fontSize: 16,
                    color: toggleValue
                      ? 'rgb(250, 250, 250)'
                      : 'rgb(80, 80, 80)',
                    fontWeight: toggleValue ? 'bold' : 'normal',
                    paddingRight: !toggleValue ? 3 : 0,
                  }}>
                  EUR
                </Text>
              }
            />
          </View>
        </View>
        <View style={styles.separator} />
        <View style={styles.titleContainer}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.titleText}>Județ</Text>
          </View>
          <DropDownPicker
            items={counties}
            defaultValue={''}
            style={{backgroundColor: 'white'}}
            itemStyle={{
              justifyContent: 'flex-start',
              zIndex: 999,
            }}
            dropDownStyle={{backgroundColor: 'white'}}
            onChangeItem={(item) => setCounty(item.value)}
            placeholder={
              <View style={{flexDirection: 'row'}}>
                <Icon
                  color="rgb(150, 150, 150)"
                  name="location-pin"
                  type="entypo"
                  size={24}
                />
                <Text style={{marginLeft: 5, fontSize: 15, paddingTop: 2}}>
                  Alege județul
                </Text>
              </View>
            }
          />
        </View>
        <View style={styles.bottomDiv}>
          <View
            style={{flexDirection: 'row', paddingTop: 20, paddingBottom: 30}}>
            <Text style={[styles.termsText, {color: termsColor}]}>
              Sunt de acord cu Termenii și Condițiile siteului Lajumate.ro
            </Text>
            <View style={{position: 'absolute', right: 15, paddingTop: 25}}>
              <ToggleSwitch
                isOn={termsValue}
                onColor={colors.green}
                offColor={
                  !termsValue && termsColor === 'red'
                    ? termsColor
                    : 'rgb(166, 166, 166)'
                }
                size="medium"
                onToggle={(isOn) => {
                  isOn && termsColor === 'red'
                    ? setTermsColor('rgba(0, 0, 0, 0.7)')
                    : null;
                  setTermsValue(isOn);
                }}
              />
            </View>
          </View>
          <TouchableOpacity
            style={styles.addButton}
            activeOpacity={0.7}
            onPress={() =>
              addAd(
                {
                  images,
                  title,
                  description,
                  price,
                  currency,
                  county,
                },
                navigation,
                termsValue,
                setTermsColor,
              )
            }>
            <LinearGradient
              start={{x: 0.4, y: 0.5}}
              end={{x: 0.44, y: 0}}
              colors={[
                'rgb(255, 165, 30)',
                'rgb(255, 180, 30)',
                'rgb(250, 184, 47)',
              ]}
              style={styles.addButton}>
              <Text style={styles.addButtonText}>ADAUGĂ ANUNȚUL</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  headerTitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  firstText: {
    color: 'white',
    fontSize: 20,
  },
  secondText: {
    fontFamily: 'MontserratAlternates-Bold',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 12,
  },
  separator: {
    backgroundColor: 'rgb(241, 241, 241)',
    height: 10,
  },
  photoContainer: {
    justifyContent: 'center',
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
  photoButtonContainer: {
    backgroundColor: colors.green,
    height: 55,
    width: 55,
    borderRadius: 30,
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
  },
  addImagesText: {
    color: colors.green,
    fontSize: 16,
    alignSelf: 'center',
    paddingTop: 8,
    paddingBottom: 25,
  },
  titleContainer: {
    paddingHorizontal: 15,
  },
  titleText: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingTop: 10,
    paddingBottom: 6,
  },
  titleCountText: {
    fontSize: 11,
    color: 'rgb(140, 140, 140)',
    paddingTop: 15,
    paddingRight: 10,
  },
  titleInput: {
    borderWidth: 1,
    borderColor: 'rgba(217, 217, 217, 0.8)',
    borderRadius: 15,
    height: 48,
    marginBottom: 15,
    paddingLeft: 15,
  },
  descriptionInput: {
    height: 100,
  },
  priceInput: {
    borderWidth: 1,
    borderColor: 'rgba(217, 217, 217, 0.8)',
    borderRadius: 25,
    height: 42,
    width: '68%',
    marginBottom: 15,
    paddingLeft: 15,
  },
  dropdownContainer: {
    height: 42,
    backgroundColor: 'white',
    color: 'white',
  },
  bottomDiv: {
    backgroundColor: 'rgb(241, 241, 241)',
    marginTop: 15,
    height: 155,
  },
  addButton: {
    height: 50,
    width: 210,
    borderRadius: 60,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignSelf: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
  },
  addButtonText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
  },
  termsText: {
    textAlign: 'right',
    fontSize: 13,
    paddingRight: 80,
    paddingLeft: 20,
    color: 'rgba(0, 0, 0, 0.7)',
  },
});

export default AddAd;
