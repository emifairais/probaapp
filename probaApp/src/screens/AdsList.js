import * as React from 'react';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  ActivityIndicator,
} from 'react-native';
import Header from '../containers/Header';
import AdCard from '../components/AdCard';
import EmptyList from '../components/EmptyList';
import {Icon} from 'react-native-elements';
import {getAds} from '../services/ads-services';
import {colors} from '../configs/colors';

function AdsList(props) {
  const {navigation} = props;
  const [ads, setAds] = React.useState('');
  const [refreshing, setRefreshing] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    setLoading(true);
    getAds(setAds, setLoading);
  }, []);

  const handleRefresh = () => {
    setRefreshing(true);
    getAds(setAds, setRefreshing);
  };

  return (
    <View
      style={{
        height: Dimensions.get('window').height + StatusBar.currentHeight,
        backgroundColor: 'white',
      }}>
      <Header
        centerComponent={
          <View style={styles.headerTitle}>
            <Image
              source={require('../assets/images/lajumate.ro.png')}
              style={styles.icon}
            />
            <Text>
              <Text style={styles.firstText} medium>
                lajumate
              </Text>
              <Text style={styles.secondText} medium>
                .ro
              </Text>
            </Text>
          </View>
        }
      />
      {loading && ads.length < 1 ? (
        <ActivityIndicator
          size="large"
          color={colors.green}
          style={{paddingTop: 100}}
        />
      ) : ads.length < 1 ? (
        <EmptyList />
      ) : (
        <FlatList
          data={ads}
          keyExtractor={(item) => item.id.toString()}
          renderItem={(ad) => (
            <AdCard
              component={TouchableWithoutFeedback}
              ad={ad.item}
              style={styles.adCard}
              hasDescription={false}
              navigation={navigation}
              isListItem={true}
              sliderHeight={200}
            />
          )}
          refreshing={refreshing}
          onRefresh={handleRefresh}
          showsVerticalScrollIndicator={true}
        />
      )}
      <TouchableOpacity
        style={styles.addButton}
        activeOpacity={0.8}
        onPress={() => navigation.navigate('AddAd')}>
        <Icon
          name="plus"
          size={33}
          type="antdesign"
          color="white"
          style={styles.plusIcon}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  headerTitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  firstText: {
    color: 'white',
    fontFamily: 'MontserratAlternates-Bold',
    fontSize: 19,
  },
  secondText: {
    fontFamily: 'MontserratAlternates-Bold',
    color: 'rgba(255, 255, 255, 0.7)',
    fontSize: 12,
  },
  icon: {
    height: 38,
    width: 38,
    borderRadius: 20,
    marginRight: 2,
  },
  addButton: {
    width: 54,
    height: 54,
    borderRadius: 27,
    backgroundColor: 'rgb(255, 152, 0)',
    position: 'absolute',
    bottom: 30,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 2,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 16,
  },
});

export default AdsList;
