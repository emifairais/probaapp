import * as React from 'react';
import {View, StyleSheet, StatusBar} from 'react-native';
import {Icon} from 'react-native-elements';
import {SliderBox} from 'react-native-image-slider-box';
import {colors} from '../configs/colors';
import {baseURL} from '../configs/constant';

function Card(props) {
  const {navigation, route} = props;
  const ad = route.params.data;

  const urlImages = [];
  ad.images.forEach((url) => {
    const fullUrl = baseURL + '/storage/' + url;
    urlImages.push(fullUrl);
  });

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor="black" barStyle="light-content" />
      <View style={styles.arrowIcon}>
        <Icon
          color="rgba(235, 235, 235, 0.9)"
          name="keyboard-backspace"
          type="material-community"
          size={38}
          onPress={() => navigation.goBack()}
        />
      </View>
      <SliderBox
        images={urlImages}
        imageLoadingColor={colors.green}
        sliderBoxHeight={300}
        dotColor={colors.green}
        paginationBoxVerticalPadding={7}
        dotStyle={styles.dotStyle}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    height: '100%',
    justifyContent: 'center',
  },
  arrowIcon: {
    position: 'absolute',
    top: 70,
    left: 15,
  },
  dotStyle: {
    width: 8,
    height: 8,
    borderRadius: 8,
    marginHorizontal: -5,
  },
});

export default Card;
