import * as React from 'react';
import {Dimensions} from 'react-native';
import {maxImages} from '../configs/constant';
import ImageCard from '../components/ImageCard';

export const updateImagesCards = (newImages, setImagesCards) => {
  const imageCardsArray = [];

  for (let i = 0; i < maxImages; i++) {
    const imageObj = {};
    imageObj.imagePath = newImages[i] ? newImages[i] : '';
    imageObj.size = Dimensions.get('window').width / 3.42;
    imageObj.type = newImages[i]
      ? 'image'
      : newImages.length < 9 && newImages[i - 1] !== undefined && !newImages[i]
      ? 'add'
      : '';
    imageCardsArray.push(imageObj);
  }

  setImagesCards(imageCardsArray);
};

export const renderCards = (imagesCards, chooseImage) => {
  return imagesCards.map((image, index) => {
    return (
      <ImageCard
        imagePath={image.imagePath}
        size={image.size}
        type={image.type}
        key={index}
        index={index}
        addImage={chooseImage}
      />
    );
  });
};
