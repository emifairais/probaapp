import axios from 'axios';
import {baseURL} from '../configs/constant';
import {Alert} from 'react-native';

let isValid = true;

export const addAd = async (params, navigation, termsValue, setTermsColor) => {
  if (!termsValue) {
    Alert.alert(
      'Eroare adăugare',
      'Câmpul Termeni și Condiții este obligatoriu!',
    );
    setTermsColor('red');
    return null;
  }
  Object.keys(params).every((index) => {
    if (params.images.length < 1 && index === 'images') {
      Alert.alert('Eroare adăugare', 'Vă rugăm atașați imagini anunțului!');
      isValid = false;
      return false;
    } else if (params.index === undefined && index !== 'images') {
      Alert.alert('Eroare adăugare', 'Toate câmpurile sunt obligatorii!');
      isValid = false;
      return false;
    }
  });
  if (!isValid) {
    return null;
  }
  const url = baseURL + '/add-ad';
  const formData = new FormData();
  params.images.forEach((image_file) => {
    formData.append('file[]', {
      uri: image_file.uri,
      type: image_file.type,
      name: image_file.fileName,
    });
  });
  formData.append('title', params.title);
  formData.append('description', params.description);
  formData.append('price', params.price);
  formData.append('currency', params.currency);
  formData.append('county', params.county);
  const response = await axios({method: 'post', url: url, data: formData});
  const data = response.data;
  if (data.code) {
    Alert.alert(data.title, data.message);
    if (data.code !== 'image.error' && data.code !== 'ad.error') {
      navigation.goBack();
      setTermsColor('rgba(0, 0, 0, 0.7)');
    }
  }
};

export const getAds = async (setAds, setLoading) => {
  const url = baseURL + '/get-ads';
  const response = await axios.get(url);
  const data = response.data;
  if (data.code) {
    Alert.alert(data.title, data.message);
    if (data.code !== 'image.error' && data.code !== 'ad.error') {
      navigation.goBack();
    }
  } else {
    setAds(data);
    setLoading(false);
  }
};
